import re
import os
import bs4
import json
import errno 
import markdown2

from math import log, inf as INFINITY
from operator import itemgetter
from collections import defaultdict

WORD_REGEX = re.compile('\w+')

def mkdir(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise  


class MaterialNode:
    def __init__(self, name):
        self.name = name
        self.content = None
        self.children = MaterialTree()
        
    def add_child(self, child):
        if self.children is None:
            self.children = MaterialTree()
        self.children.add_child(child)
            
    def dump(self):
        data = {'name': self.name}
        if not self.children.is_empty:
            data['children'] = self.children.dump()
        if self.content is not None:
            data['content'] = self.content
        return data


class MaterialTree:
    def __init__(self):
        self.children = []
        self.is_empty = True
        
    def create(self, content, *path):
        if len(path) > 0:    
            matches = tuple(filter(lambda child: child.name==path[0], self.children))
            if len(matches) == 0:
                match = self.add_child(path[0])
            else:
                match = matches[0]
                
            if len(path) > 1:
                match.children.create(content, *path[1:])
            else:
                match.content = content
            
        return match
        
    def add_child(self, name):
        child = MaterialNode(name)
        self.children.append(child)
        self.is_empty = False
        return child
        
    def dump(self, indent=None):
        return [data.dump() for data in self.children]
    
    def json(self, indent=None):
        return json.dumps(self.dump(), indent=indent)


class Markdown:
    def __init__(self, text, tags_pounds={}, markdown_folder=None):
        self.text = text
        self.html = markdown2.markdown(text, extras=['fenced-code-blocks', 'tables'])
        self.tags_pounds = tags_pounds
        self.markdown_folder = markdown_folder
        # lazy loading
        self._soup = None
        self._tags = None


    @property
    def soup(self):
        if self._soup is None:
            self._soup = bs4.BeautifulSoup(self.html, "html.parser")
        return self._soup
    
    @property
    def tags(self):
        if self._tags is None:
            self._tags = self.extract_tags()
        return self._tags
    
    def top_tags(self, limit=5):
        return list(sorted(
            self.tags.items(),
            key=itemgetter(1), 
            reverse=True
        ))[:limit]
        
    
    def extract_tags(self):
        tags = defaultdict(int)
        
        for tag, pound in self.tags_pounds.items():
            for heading in self.soup.select(tag):
                for word in WORD_REGEX.findall(heading.get_text()):
                    tags[word] +=  pound
                    #store[word] += pound
                    
        return tags
    
    
    def save(self, output):
        try:
            with open(output, 'w') as out:
                out.write(self.html)
        except FileNotFoundError:
            dir, _ = os.path.split(output)
            mkdir(dir)
            self.save(output)


class MarkdownFolder:
    def __init__(self):
        self.markdowns = []
        self.tree = MaterialTree()
        
    def add(self, path, mdfile):
        self.markdowns.append(mdfile)
        print(path)
        self.tree.create(path, *outpath)
    
    def all_tags(self):
        tags = defaultdict(int)
        for mdfile in self.markdowns:
            for tag, pound in mdfile.tags.items():
                tags[tag] += pound
        return tags                


class MarkdownLoader:
    def __init__(self, heading_max_depth=5, max_keywords=5, exclude_keywords=()):
        self.heading_max_depth = heading_max_depth
        self.max_keywords = max_keywords
        self.tags_pounds = {
            'p': 1,
            'div': 1,
            **{
                'h%s'%k: k for k in range(1, heading_max_depth+1)
            },
            **{
                exclusion: INFINITY for exclusion in exclude_keywords
            }
        }


    def read_markdown(self, path, store: defaultdict(int)):
        source = open(path).read()
        html = markdown2.markdown(source)
        soup = bs4.BeautifulSoup(html.lower(), "html.parser")
        
        tags = defaultdict(int)
        for tag, pound in self.container_tags.items():
            for heading in soup.select(tag):
                for word in WORD_REGEX.findall(heading.get_text()):
                    tags[word] +=  pound
                    store[word] += pound
                    
        return tags

    def ponder_tags(self, tags, all_tags):
        results = {}
        for key, val in tags.items():
            results[key] = tags[key] / all_tags[key]
        return results
    
    
    def open_markdown(self, path):
        text = open(path).read()
        return Markdown(text, self.tags_pounds)

    
    def convert_folder(self, input, output, flat=False):
        prefix_length = len(os.path.normpath(input).split(os.path.sep))
        markdown_folder = MarkdownFolder()
        
        for dirname, subdirectories, filenames in os.walk(input, topdown=True):
            if not '.git' in dirname:
                filenames = tuple(filter(lambda filename: filename.lower().endswith('.md'), filenames))
                if(len(filenames)>0):
                    path = os.path.normpath(dirname).split(os.path.sep)[prefix_length:]

                    for filename in filenames:
                        inputpath = os.path.normpath(os.path.join(dirname, filename))
                        
                        outpath = path + [ filename[:-3] ]
                        outputpath = os.path.sep.join((output, *outpath))
                        
                        mdfile = self.open_markdown(inputpath)
                        mdfile.save(output)
                        
                        markdown_folder.add(os.path.sep.join(outpath), mdfile)
                        
        
        with open(os.path.join(output, 'tree.json'), 'w') as treefile:
            json.dump(markdown_tree.dump(), treefile, indent=4)
        
        tags_files = defaultdict(list)
        tags_store = defaultdict(int)
        for file in files:
            for tag in file[1].top_tags():
                tags_files[tag[0]].append((file[0], tag[1]))
                
        with open(os.path.join(output, 'tags_files.json'), 'w') as tagsfiles:
            json.dump(tags_files, tagsfiles, indent=4)


import argparse
def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('input', help='input folder containing markdowns')
    parser.add_argument('-o', '--output', default='/usr/share/nginx/html/assets/docs',
                        help='output folder for html and analysis')

    args = parser.parse_args()
    
    loader = MarkdownLoader()
    loader.convert_folder(args.input, args.output, flat=False)
    
if __name__ == '__main__':
    main()