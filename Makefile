help:
	@echo "no doc for now!"

init:
	rm -rf .env
	virtualenv .env -p /usr/bin/python3
	make install

install:
	.env/bin/pip install nuitka
	.env/bin/pip install -r wiki_builder/requirements

test-cli:
	.env/bin/python build-wiki/build-wiki.py wiki -o docs

build-cli: ## require nuitka
	.env/bin/python -m nuitka wiki_builder --follow-imports --plugin-enable=numpy --standalone --include-package wiki_builder

test-cli-build:
	wiki_builder.dist/wiki_builder toto

image:
	docker build -t markdown-wiki .

test-image:
	docker run -it markdown-wiki