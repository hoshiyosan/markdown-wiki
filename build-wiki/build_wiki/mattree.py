import os
import json

class MaterialNode:
    def __init__(self, name):
        self.name = name
        self.content = None
        self.children = MaterialTree()
        
    def add_child(self, child):
        if self.children is None:
            self.children = MaterialTree()
        self.children.add_child(child)
            
    def dump(self):
        data = {'name': self.name}
        if not self.children.is_empty:
            data['children'] = self.children.dump()
        if self.content is not None:
            data['content'] = self.content
        return data


class MaterialTree:
    def __init__(self):
        self.children = []
        self.is_empty = True
        
    def create(self, content, *path):
        if len(path) > 0:    
            matches = tuple(filter(lambda child: child.name==path[0], self.children))
            if len(matches) == 0:
                match = self.add_child(path[0])
            else:
                match = matches[0]
                
            if len(path) > 1:
                match.children.create(content, *path[1:])
            else:
                match.content = content
            
        return match
        
    def add_child(self, name):
        child = MaterialNode(name)
        self.children.append(child)
        self.is_empty = False
        return child
        
    def dump(self, indent=None):
        return [data.dump() for data in self.children]
    
    def save(self, directory, indent=None):
        with open(os.path.join(directory, 'tree.json'), 'w') as treefile:
            json.dump(self.dump(), treefile, indent=indent)        
