class Logger:
    enabled = True
    
    def log(self, *text):
        if self.enabled:
            print(*text)