import argparse
from .mdfolder import MarkdownFolder

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('input', help='input folder containing markdowns')
    parser.add_argument('-o', '--output', default='/usr/share/nginx/html/assets/docs',
                        help='output folder for html and analysis')

    args = parser.parse_args()
    
    mdfolder = MarkdownFolder(args.input)
    mdfolder.save_wiki(args.output)
    
    
if __name__ == '__main__':
    main()