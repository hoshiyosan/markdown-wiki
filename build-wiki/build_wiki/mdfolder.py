import os
import json
from collections import defaultdict
from operator import itemgetter

from .logger import Logger
from .mdfile import MarkdownFile
from .mattree import MaterialTree
from .non_pertinents import NON_PERTINENTS

class MarkdownFolder:
    def __init__(self, input_path):
        self.logger = Logger()
        self.markdowns = []
        self.tree = MaterialTree()
        
        # lazy loading
        self._all_tags_loaded = False
        self._all_tags = defaultdict(int)
        
        # load files tags after non pertinents tags has been loading
        self.load_files(input_path)        
    
        
    def load_files(self, input_path):
        path = os.path.abspath(input_path)
        
        if not os.path.isdir(input_path):
            raise FileNotFoundError("No such directory %s"%path)
        
        self.logger.log('loading markdown in folder', path)
        
        for folder, _, files in os.walk(path):
            for file in files:
                if file.lower().endswith('.md'):
                    rel_path = folder.replace(path, '').strip('/')
                    rel_path_nodes = rel_path.split('/') if rel_path else []
                    self.markdowns.append(MarkdownFile(folder, rel_path, file))
                    # feed material tree
                    self.tree.create(os.path.join(rel_path, file[:-3]), *rel_path_nodes, file[:-3])
    
    
    def all_tags(self):
        if self._all_tags_loaded is False:
            for mdfile in self.markdowns:
                for tag, pound in mdfile.tags.items():
                    self._all_tags[tag] += pound
            self._all_tags_loaded = True
        return self._all_tags
                
        
    def save_as_html(self, output_path):
        for mdfile in self.markdowns:
            mdfile.save(output_path)
            
    
    def save_files_by_tags(self, output_path, indent=None):
        files_by_tag = defaultdict(list)
        occurences = self.all_tags()
        for file in self.markdowns:
            for tag, pound in file.top_tags(occurences=occurences, exclusion=NON_PERTINENTS, limit=50):
                files_by_tag[tag].append((file.path, pound))
                
        with open(os.path.join(output_path, 'files_by_tag.json'), 'w') as fbtfile:
            json.dump(files_by_tag, fbtfile, indent=indent)
            
            
    def save_tags(self, output_path, indent=None):
        tags = list(self.all_tags().keys())
        with open(os.path.join(output_path, 'tags.json'), 'w') as tagsfile:
            json.dump(tags, tagsfile, indent=indent)
            
    
    def save_top_tags(self, output_path, limit=7, indent=None):
        top_tags_by_file = {}
        occurences = self.all_tags()
        for mdfile in self.markdowns:
            top_tags_by_file[mdfile.path] = tuple(map(
                itemgetter(0), 
                mdfile.top_tags(occurences, limit=limit)
            ))
        
        with open(os.path.join(output_path, 'top_tags.json'), 'w') as toptagsfile:
            json.dump(top_tags_by_file, toptagsfile, indent=indent)
            
            
    def save_metadata(self, output_path, indent=4):
        metadata = {}
        for mdfile in self.markdowns:
            metadata[mdfile.path] = mdfile.data
        
        with open(os.path.join(output_path, 'metadata.json'), 'w') as metadatafile:
            json.dump(metadata, metadatafile, indent=indent)
            
      
    def save_wiki(self, output_path):
        self.save_as_html(output_path)
        self.tree.save(output_path, indent=4)
        self.save_tags(output_path, indent=4)
        self.save_files_by_tags(output_path, indent=4)
        self.save_top_tags(output_path, indent=4)
        self.save_metadata(output_path, indent=4)
