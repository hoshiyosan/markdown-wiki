import os
import re
import bs4
import markdown2
from operator import itemgetter
from collections import defaultdict

from .settings import DEFAULT_TAGS_POUNDS
from .logger import Logger
from .mkdir import mkdir

WORD_REGEX = re.compile('\w+')

class MarkdownFile:
    def __init__(self, input_folder, rel_path, name, tags_pounds=DEFAULT_TAGS_POUNDS):
        # util for debug purposes
        self.logger = Logger()
        # path properties
        self.folder = rel_path
        self.name = name[:-3]
        self.path = os.path.join(rel_path, name[:-3])
        ####
        self.tags_pounds = tags_pounds
        # computed attributes
        self.text = open(os.path.join(input_folder, name)).read()
        self.html = markdown2.markdown(self.text, extras=['fenced-code-blocks', 'tables'])
        self.soup = bs4.BeautifulSoup(markdown2.markdown(self.html), "html.parser")
        # attributes for lazy loading
        self._tags = None
        self._title = None
    
    @property
    def title(self):
        h1s = self.soup.select('h1')
        self._title = h1s[0].get_text() if len(h1s) > 0 else None
        return self._title
        
    @property
    def tags(self):
        if self._tags is None:
            self._tags = defaultdict(int)
            for htmltag, pound in self.tags_pounds.items():
                matches = tuple(map(lambda el: el.get_text(), self.soup.select(htmltag)))
                for match in matches:
                    for tag in WORD_REGEX.findall(match):
                        self._tags[tag.lower()] += pound
            # populate tags
        return self._tags
    
    def poundered_tags(self, occurences={}, exclusion={}):
        tags = {}
        for tag, pound in self.tags.items():
            virtualenv_occurences = exclusion[tag] if exclusion.get(tag) else occurences.get(tag, pound)
            tags[tag] = pound / virtualenv_occurences
        return tags
    
    def top_tags(self, occurences={}, exclusion={}, limit=5):
        return tuple(sorted(
            self.poundered_tags(occurences=occurences, exclusion=exclusion).items(),
            key=itemgetter(1),
            reverse=True
        ))[:limit]
        
    def save(self, directory):
        output_directory = os.path.abspath(
            os.path.sep.join((directory, self.folder))
        )
        try:
            mkdir(output_directory)
        except FileExistsError:
            pass
            
        with open(os.path.join(output_directory, self.name), 'w') as out:
            out.write(self.html)
    
    @property
    def data(self):
        return {
            'name': self.name,
            'title': self.title,
            'path': self.path
        }
