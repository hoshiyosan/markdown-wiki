import re
import os
import bs4
import json
import time
import requests

from tqdm import trange
from operator import itemgetter
from collections import defaultdict


WORD_REGEX = re.compile('\w+')
NON_PERTINENTS_PATH = './extractions/non-pertinents'


def get_articles_words(counter: defaultdict(int), progress):
    r = requests.get("https://en.wikipedia.org/wiki/Special:Random")
    assert r.status_code == 200, "error %s loading random article"%r.status_code
    
    soup = bs4.BeautifulSoup(r.text, "html.parser")
    title = soup.select('h1')[0].get_text()[:30]
    progress.set_description(title + ' '*(30-len(title)))
    progress.refresh()
    
    content = soup.select('.mw-content-ltr')[0]
    # prepare exclusion
    for tag in ('style', 'script', 'a'):
        for unwanted in content.select(tag):
            unwanted.extract()
    
    # exclude unwanted tags
    text = content.get_text().replace('\n', ' ')
    words = WORD_REGEX.findall(text)
    
    for word in words:
        try:
            float(word)
        except ValueError:
            counter[word.lower()] += 1  


def new_sample(N=500):
    counter = defaultdict(int)

    progress = trange(N, desc=None, leave=True)
    for article in progress:
        try:
            get_articles_words(counter, progress)
        except:
            time.sleep(1)

    nonpertinents = sorted(counter.items(), key=itemgetter(1), reverse=True)[:500]
    ts = int(time.time())
    with open('%s/%s.json'%(NON_PERTINENTS_PATH, ts), 'w') as nonpertinentsfile:
        json.dump(nonpertinents, nonpertinentsfile)
        
        
def summarize(limit=500):
    files = os.listdir(NON_PERTINENTS_PATH)
    frequencies = defaultdict(int)
    
    for file in files:
        for word, occurencies in json.load(open(os.path.join(NON_PERTINENTS_PATH, file))):
            frequencies[word] += occurencies
            
    return dict(sorted(frequencies.items(), key=itemgetter(1), reverse=True)[:limit])


with open('extractions/non-pertinents.json', 'w') as file:
    json.dump(summarize(), file)
