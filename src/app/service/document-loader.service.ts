import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable, BehaviorSubject, forkJoin } from 'rxjs';
import { Document } from '../interface/document';
import { DocumentMetadata } from '../interface/document-metadata';

@Injectable({
  providedIn: 'root'
})
export class DocumentLoaderService {
  private selectionSubject = new BehaviorSubject<Document>(null);
  public selection = this.selectionSubject.asObservable();

  private filesMetadataSubject = new BehaviorSubject<DocumentMetadata[]>([])
  public filesMetadata = this.filesMetadataSubject.asObservable();

  private treeSubject = new BehaviorSubject([]);
  public tree = this.treeSubject.asObservable();

  private tagsSubject = new BehaviorSubject<string[]>([]);
  public tags = this.tagsSubject.asObservable();

  private filesByTagSubject = new BehaviorSubject({});
  public filesByTag = this.filesByTagSubject.asObservable();


  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {
    this.loadTree();
    this.loadTags();
    this.loadFilesByTag();
    this.loadFilesMetadata();
  }

  /* initializers */

  loadFilesMetadata() {
    this.http.get('assets/docs/metadata.json').subscribe((metadata: DocumentMetadata[]) => {
      this.filesMetadataSubject.next(metadata);
    })
  }

  loadFilesByTag() {
    this.http.get('assets/docs/files_by_tag.json').subscribe((fbt: object) => {
      this.filesByTagSubject.next(fbt);
    })
  }

  loadTree() {
    this.http.get('assets/docs/tree.json').subscribe((tree: any[]) => {
      this.treeSubject.next(tree);
    })
  }

  loadTags() {
    this.http.get('assets/docs/tags.json').subscribe((tags: string[]) => {
      console.log(tags);
      this.tagsSubject.next(tags);
    })
  }

  /* set selected markdown */

  openMarkdown(path: string) {
    return this.http.get('assets/docs/' + path, { responseType: 'text' }).subscribe(
      (text: string) => {
        this.selectionSubject.next({
          path: path,
          html: this.sanitizer.bypassSecurityTrustHtml(text)
        })
        return this.selectionSubject.value;
      })
  }

  /* search functions */

  inArray(tag: string, terms: string[]) {
    for (let term of terms) {
      if (tag.includes(term)) {
        return true;
      }
    }
    return false;
  }


  getMatchingTags(search: string): Observable<string[]> {
    return this.tags.pipe(
      map(tags => {
        const terms = search.split(' ');
        const matchingTags = tags.filter((tag: string) => this.inArray(tag, terms));
        return matchingTags;
      })
    )
  }


  workoutFilePertinenceForTags(tags: string[]) {
    console.log('working out pertinences');
    const filesByTag = this.filesByTagSubject.value;
    const filesMetadata = this.filesMetadataSubject.value;

    const filePound = {};
    for (let tag of tags) {
      for (let filedata of filesByTag[tag]) {
        if (filePound[filedata[0]] === undefined) {
          filePound[filedata[0]] = filedata[1];
        } else {
          filePound[filedata[0]] += filedata[1];
        }
      }
    }

    let files = Object.entries(filePound)
      .sort((t1, t2) => (t1[1] > t2[1]) ? -1 : 1)
      .map(item => filesMetadata[item[0]]);

    console.log(files);
    return files;
  }

  query(search: string) {
    return this.getMatchingTags(search).pipe(
      map((tags: string[]) => this.workoutFilePertinenceForTags(tags))
    )
  }
}