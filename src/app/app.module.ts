import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { FilesTreeComponent } from './widget/files-tree/files-tree.component';
import { DocumentLoaderComponent } from './widget/document-loader/document-loader.component';
import { HttpClientModule } from '@angular/common/http';
import { ToolbarComponent } from './widget/toolbar/toolbar.component';
import { FilesExplorerComponent } from './widget/files-explorer/files-explorer.component';
import { FormsModule } from '@angular/forms';
import { SearchResultsComponent } from './widget/search-results/search-results.component';

@NgModule({
  declarations: [
    AppComponent,
    FilesTreeComponent,
    DocumentLoaderComponent,
    ToolbarComponent,
    FilesExplorerComponent,
    SearchResultsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
