import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DocumentLoaderService } from 'src/app/service/document-loader.service';
import { MatTabGroup } from '@angular/material/tabs';

const TREE_TAB = "Tree";
const RESULTS_TAB = "Results";

@Component({
  selector: 'app-files-explorer',
  templateUrl: './files-explorer.component.html',
  styleUrls: ['./files-explorer.component.scss']
})
export class FilesExplorerComponent {
  @ViewChild('tabs', { static: false }) tabGroup: MatTabGroup;
  //
  public selectedTab: string = TREE_TAB;
  public search: string = "";
  public results: string[] = [];

  constructor(private docs: DocumentLoaderService) { }

  onSearch() {
    this.docs.query(this.search).subscribe(results => {
      this.results = results;
      this.tabGroup.selectedIndex = 1;
    })
  }

}
