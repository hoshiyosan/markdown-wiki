import { Component, OnInit, Input } from '@angular/core';
import { DocumentLoaderService } from 'src/app/service/document-loader.service';
import { DocumentMetadata } from 'src/app/interface/document-metadata';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit {
  @Input() results: DocumentMetadata[] = [];

  constructor(private doc: DocumentLoaderService) { }

  ngOnInit() {
  }

  openMarkdown(path: string) {
    this.doc.openMarkdown(path);
  }

}
