import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { DocumentLoaderService } from 'src/app/service/document-loader.service';

interface MarkdownNode {
  name: string;
  content?: string;
  children?: MarkdownNode[];
}

@Component({
  selector: 'app-files-tree',
  templateUrl: './files-tree.component.html',
  styleUrls: ['./files-tree.component.scss']
})
export class FilesTreeComponent {

  treeControl = new NestedTreeControl<MarkdownNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<MarkdownNode>();

  constructor(private docs: DocumentLoaderService) {
    this.dataSource.data = [];
    docs.tree.subscribe((tree: MarkdownNode[]) => {
      this.dataSource.data = tree;
    })
  }

  selectFile(node: MarkdownNode) {
    this.docs.openMarkdown(node.content);
  }

  hasChild = (_: number, node: MarkdownNode) => !!node.children && node.children.length > 0;
}
