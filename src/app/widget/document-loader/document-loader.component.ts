import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { DocumentLoaderService } from 'src/app/service/document-loader.service';
import { Document } from 'src/app/interface/document';

@Component({
  selector: 'app-document-loader',
  templateUrl: './document-loader.component.html',
  styleUrls: ['./pygments.scss', './document-loader.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DocumentLoaderComponent implements OnInit {
  private _path: string = null;
  document: Document;

  constructor(private docs: DocumentLoaderService) { }

  ngOnInit() {
    this.docs.selection.subscribe((doc: Document) => {
      console.log('selection changed');
      this.document = doc;
    })
    this.docs.openMarkdown("aliases");
  }

}
