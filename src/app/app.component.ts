import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'interface';
  selectedFile = null;
  selection = null;

  onSelectionChange() {
    console.log('change in app: ' + this.selection);
  }
}
