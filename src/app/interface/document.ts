import { SafeHtml } from '@angular/platform-browser';

export class Document {
    path: string;
    html: SafeHtml;
}
