import { SafeHtml } from '@angular/platform-browser';

export class DocumentMetadata {
    name: string;
    title: string;
    path: string;
    html: SafeHtml;
}
