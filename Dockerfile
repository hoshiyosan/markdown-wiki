###############################################
FROM node:alpine AS ui-builder

WORKDIR /app
COPY . /app

RUN npm install && \
    npm run build
    
###############################################
#FROM python:3.6-alpine as wiki-builder

#RUN apk add gcc
#RUN apk add musl-dev
#RUN apk add chrpath

#RUN pip3 install twine
#RUN pip3 install pygments markdown2 bs4

#WORKDIR /app
#COPY build-wiki /app/build-wiki

#RUN nuitka3 --standalone --follow-imports --include-package=pygments.styles --include-package=pygments.lexers --include-package=pygments.formatters build-wiki/build-wiki.py


###############################################
FROM nginx:alpine

## install wiki builder in container
RUN apk add python3
COPY build-wiki ./build-wiki
RUN pip3 install ./build-wiki && rm -rf ./build-wiki

## install built ui in nginx folder
COPY --from=ui-builder /app/dist/* /usr/share/nginx/html/
